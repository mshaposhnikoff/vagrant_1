zabbix-agent playbook
==============
This playbook install zabbix-agent

Requirements
------------
### Supported Operating Systems

- RedHat-family Linux Distributions x64

### Playbooks

Attributes
----------
### Recommended tunables

- `zabbix_server`
  - String. Zabbix server host or IP

- `zabbix_server_port`
  - String. Zabbix server port

- `listen_port`
  - String. Zabbix agent port

- `ansible_nodename`
  - String. Zabbix agent Hostname

Tags
----------
### Supported tags

- `zabbix_agent_download`
  - Download artifacts
- `zabbix_agent_install`
  - Download and install zabbix-agent
- `zabbix_agent_remove`
  - Remove zabbix-agent
- `zabbix_agent_restart`
  - Restart zabbix-agent
- `add_host_to_server`
  - Add host to the Zabbix Server

To install custom user parameters for monitoring use nexts tags:

- `userparameters_adobe_cq_dispatcher`
- `userparameters_adobe_cq_author`
- `userparameters_adobe_cq_publish`
- `userparameters_hybris`
- `userparameters_fri`
- `userparameters_oauth`
- `userparameters_odc`
- `userparameters_pcs`
- `userparameters_solr`
- `userparameters_jenkins`
- `userparameters_nexus`

Usage
-----
Create `zabbix-agent.yml` playbook and execute it:

```---
- hosts: all
  user: "{{ ansible_ssh_user }}"

  roles:
    - zabbix-agent
```

Example
-------

For FGL ENV's use next settings:

zabbix_server: 10.6.131.37
zabbix_server_port: 4501
listen_port: 4501

ansible-playbook -i inventory/kyiv/epam_uat2 zabbix_agent.yml --tags zabbix_agent_install

ansible-playbook -i inventory/fgl_env_1 -l cq_publish zabbix_agent.yml --tags zabbix_agent_install --extra-vars="zabbix_server=10.6.131.37 zabbix_server_port=4501 zabbix_agent_port=4501 ansible_nodename=env1.publish.fmcms02d.fglcorporate.net"
